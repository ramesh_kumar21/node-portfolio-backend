//importing mongoose for db acess
const mongoose = require('mongoose');

//importing bcrypt for encrytping password
const bcrypt = require('bcrypt');

//importing jwt
const jwt = require("jsonwebtoken");

//date dependency
const date_now =  new Date();


// importing user model
const User = require('../../models/user/user_model');

//user sign_up function
exports.user_create = (req, res, next) => {
	const username=User.findOne({ user_name: req.body.username });
	User.find({ email: req.body.email })
	  .exec()
	  .then(user => {
		if (user.length >= 1) {
		  return res.status(409).json({
			message: "Mail exists"
			});
		}else {
		  User.find({ user_name: req.body.username })
	  .exec()
	  .then(uname => {
		if (uname.length >= 1) {
		  return res.status(409).json({
			message: "Username exists"
			});
		}else {
		  bcrypt.hash(req.body.password, 10, (err, hash) => {
				if (err) {
					return res.status(500).json({
					error: err
					});
				} else {
					const user = new User({
					_id: new mongoose.Types.ObjectId(),
					email: req.body.email,
					password: hash,
					user_name: req.body.username,
					created_date:date_now
					});
					user
					.save()
					.then(result => {
						console.log(result);
						res.status(201).json({
						message: "User created"
						});
					})
					.catch(err => {
						console.log(err);
						res.status(500).json({
						error: err
						});
					});
				}
				});
		}
	  });
		}
	  });
  }

	


//user login function
exports.user_login = (req, res, next) => {
User.find({ email: req.body.email })
	.exec()
	.then(user => {
	if (user.length < 1) {
		return res.status(401).json({
		message: "Auth failed"
		});
	}
	bcrypt.compare(req.body.password, user[0].password, (err, result) => {
		if (err) {
		return res.status(401).json({
			message: "Auth failed"
		});
		}
		if (result) {
		const token = jwt.sign(
			{
			email: user[0].email,
			userId: user[0]._id
			},
			process.env.JWT_KEY,
			{
				expiresIn: "1h"
			}
		);
		return res.status(200).json({
			message: "Auth successful",
			token: token
		});
		}
		res.status(401).json({
		message: "Auth failed"
		});
	});
	})
	.catch(err => {
	console.log(err);
	res.status(500).json({
		error: err
	});
	});
}