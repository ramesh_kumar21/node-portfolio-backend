//importing mongoose for db acess
const mongoose = require('mongoose');

//jwt import
const jwt = require('jsonwebtoken');

// importing user model
const Profile = require('../../models/user/profile_model');
const User = require('../../models/user/user_model');




//user profile create

exports.profile_create = (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  final_data=req.userData = decoded;
  Profile.find({ user: final_data.userId })
  .exec()
  .then(profile =>{
    if (profile.length >= 1){
      return res.status(409).json({
        message: "Profile exists"
        });
    }else{
      const profile = new Profile({
        _id: new mongoose.Types.ObjectId(),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        sex: req.body.sex,
        learning_interests:req.body.learning_interests,
        teaching_interests:req.body.teaching_interests,
        user:final_data.userId
      });
      profile
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            message: "Created Profile successfully",
            createdProfile: {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              sex: req.body.sex,
              learning_interests:req.body.learning_interests,
              teaching_interests:req.body.teaching_interests
            }
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
    });
  };


//user profile get

exports.profile_get_byid = (req, res, next) => {
  if (req.headers){
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  final_data=req.userData = decoded;
  finalid = final_data.userId
  }else{
    finalid = "None"
  }
  Profile.find({user:finalid})
    .select("firstname lastname sex learning_interests teaching_interests")
    .populate("profile")
    .exec()
    .then(profile => {
      if (!profile) {
        return res.status(404).json({
          message: "Profile not found"
        });
      }
      res.status(200).json({
        profile: profile
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};

//update profile

exports.profile_update = (req, res, next) => {
  if (req.headers){
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    final_data=req.userData = decoded;
    finalid = final_data.userId
    }else{
      finalid = "None"
    }
    console.log(req.body);
  
  Profile.update({ user: finalid }, { $set: {
    learning_interests:req.body.learning_interests,
    teaching_interests:req.body.teaching_interests,
    firstname : req.body.firstname,
    lastname:req.body.lastname,
    sex:req.body.sex
  } })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Product updated"
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};