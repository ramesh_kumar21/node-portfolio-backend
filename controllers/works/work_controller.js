//importing mongoose for db acess
const mongoose = require('mongoose');

//jwt import
const jwt = require('jsonwebtoken');

// importing user model
const Work = require('../../models/work/work_model');
const Info = require('../../models/work/bio_model');
const User = require('../../models/user/user_model');

//date dependency
const date_now =  new Date();



//Add work create

exports.work_create = (req, res, next) => {
  console.log(req.file);
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  final_data=req.userData = decoded;
      const work = new Work({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.projectname,
        companyname: req.body.companyname,
        created_date: date_now,
        about_project:req.body.about_project,
        thumbnail:req.file.path,
        languages:req.body.languages_used,
        user:final_data.userId
      });
      work
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            message: "Added Work successfully",
            AddedWork: {
                name: req.body.projectname,
                companyname: req.body.companyname,
                created_date: date_now,
                about_project:req.body.about_project,
                thumbnail:req.file.path,
                languages:req.body.languages_used,
                user:final_data.userId
            }
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    
  }


//get works
exports.get_works = (req, res, next) => {
Work.find()
    .select("name companyname about_project thumbnail languages")
    .populate("work")
    .exec()
    .then(work => {
    res.status(200).json({
        work: work
    });
    })
    .catch(err => {
    res.status(500).json({
        error: err
    });
    });
};


//add bio

exports.my_bio = (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  final_data=req.userData = decoded;
      const bio = new Info({
        _id: new mongoose.Types.ObjectId(),
        bio: req.body.bio,
        created_date: date_now,
        user:final_data.userId
      });
      bio
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            message: "Added Work successfully",
            AddedWork: {
                bio: req.body.bio,
                created_date: date_now,
                user:final_data.userId
            }
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    
  }



  //get bio
exports.get_bio = (req, res, next) => {
  Info.find()
      .select("bio")
      .populate("my_info")
      .exec()
      .then(my_info => {
      res.status(200).json({
        my_info: my_info
      });
      })
      .catch(err => {
      res.status(500).json({
          error: err
      });
      });
  };
  