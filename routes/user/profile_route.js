const express = require('express');
const router  = express.Router();
const mongoose = require('mongoose');

//import validation
const checkAuth = require('../../middleware/check-auth');

//importing controller

const ProfileController = require('../../controllers/user/profile_controller');

//user profile create route
router.post('/create',checkAuth,ProfileController.profile_create);

//user profile get
router.get('/my_profile',ProfileController.profile_get_byid);

//user profile update
router.put('/update',ProfileController.profile_update);


module.exports = router; 