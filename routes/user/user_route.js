const express = require('express');
const router  = express.Router();
const mongoose = require('mongoose');
//import validation
const checkAuth = require('../../middleware/check-auth');
//importing controller

const UserController = require('../../controllers/user/user_controller');

//user sign_up route
router.post('/sign_up',UserController.user_create);

//user login route
router.post('/login',UserController.user_login);

module.exports = router; 