const express = require('express');
const router  = express.Router();
const mongoose = require('mongoose');

//import validation
const checkAuth = require('../../middleware/check-auth');

//importing controller

const WorkContorller = require('../../controllers/works/work_controller');

//multer for image upload
const multer  = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        const dt = new Date();
      cb(null,  dt.toISOString()+file.originalname);
    }
  });
  
  const upload = multer({ storage: storage });





//work adding route
router.post('/add_work', upload.single('thumbnail'),WorkContorller.work_create);



//get all  work route
router.get('/',WorkContorller.get_works);

//add bio route
router.post('/add_bio',WorkContorller.my_bio);

//get bio route
router.get('/my_bio',WorkContorller.get_bio);

module.exports = router; 