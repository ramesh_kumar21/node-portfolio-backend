const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	_id:mongoose.Schema.Types.ObjectId,
	email:{type:String , required:true},
	password:{type:String , required:true},
	created_date:{type:String , required:true},
	user_name:{type:String , required:true}
    });

module.exports = mongoose.model('User',UserSchema);