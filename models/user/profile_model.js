const mongoose = require('mongoose');

const ProfileSchema = mongoose.Schema({
	_id:mongoose.Schema.Types.ObjectId,
    firstname: String,
    lastname: String,
    sex: String,
    learning_interests: { type : Array , "default" : [] },
    teaching_interests: { type : Array , "default" : [] },
    user:{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
    });

module.exports = mongoose.model('Profile',ProfileSchema);