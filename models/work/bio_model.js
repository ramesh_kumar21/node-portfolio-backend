const mongoose = require('mongoose');
const InfoSchema = mongoose.Schema({
	_id:mongoose.Schema.Types.ObjectId,
	bio:{type:String , required:true},
    created_date:{type:String , required:true},
	user:{type:String , required:true}
    });

module.exports = mongoose.model('Info',InfoSchema);