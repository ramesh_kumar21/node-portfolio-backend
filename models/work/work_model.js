const mongoose = require('mongoose');

const WorkSchema = mongoose.Schema({
	_id:mongoose.Schema.Types.ObjectId,
	name:{type:String , required:true},
	companyname:{type:String , required:true},
    created_date:{type:String , required:true},
	about_project:{type:String},
	thumbnail:{type:String},
	languages:{type:Array, "default" : []},
	user:{type:String , required:true}
    });

module.exports = mongoose.model('Work',WorkSchema);


